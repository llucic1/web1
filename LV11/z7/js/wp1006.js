//Regularna funkcija koja se poziva prema nazivu funkcije
function multiply() {
    var result = 3 * 4;
    console.log("3 pomnožen s 4 je ", result);
}
//poziv funkcije
multiply();

// Anonimna funkcija koja se sprema u varijablu.
var divided = function() {
    var result = 3 / 4;
    console.log("3 podijeljeno s 4 je ", result);
}
//Poziv anonimne funkcije koja je spremljena u varijablu
divided();

// Immediately Invoked funkcijski izraz.
// Pokreće se u trenutku kad browser naiđe na ovu funkciju
(function() {
    var result = 12 / 0.75;
    console.log("12 podijeljeno 0.75 je ", result);
}())
