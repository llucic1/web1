document.write("<h1>Jednostruki  i dvostruki navodnici i string vrijednosti</h1>" + "<br>");
var a = 'Pozdrav!';
var b = "Pozdrav!";

document.write(a + "<br>");
document.write(b);
document.write('<br>');

document.write("<h1>Brojevi</h1>" + "<br>");
var a = 25;
var b = 80.5;
var c = 4.25e+6;
var d = 4.25e-6;

document.write(a + "<br>");
document.write(b + "<br>");
document.write(c + "<br>");
document.write(d);
document.write('<br>');

document.write("<h1>Dijelejnje s 0 --> beskonačno</h1>" + "<br>");
document.write(16 / 0);
document.write("<br>");
document.write(-16 / 0);
document.write("<br>");
document.write(16 / -0);
document.write('<br>');

document.write("<h1>Problem kad se ne može odrediti konačni tip varijable</h1>" + "<br>");
document.write("Some text" / 2);
document.write("<br>");
document.write("Some text" / 2 + 10);
document.write("<br>");
document.write(Math.sqrt(-1));

document.write("<h1>Boolean tip varijable</h1>" + "<br>");
var isReading = true;
var isSleeping = false;
document.write(isReading + "<br>");
document.write(isSleeping);

document.write("<h1>Nedefinirana varijabla</h1>" + "<br>");
var k;
var g = "Hello World!"
document.write(k + "<br>");
document.write(g);

document.write("<h1>Null vrijednost u varijablama</h1>" + "<br>");
var a = null;
document.write(a + "<br>");
var b = "Hello World!"
document.write(b + "<br>");
b = null;
document.write(b)

document.write("<h1>Objekti</h1>" + "<br>");
var emptyObject = {};
var person = { "name": "Clark", "surname": "Kent", "age": "36" };
var car = {
    "modal": "BMW X3",
    "color": "white",
    "doors": 5
}

document.write("objekt person" + "<br>");
document.write(person + "<br>");
document.write("objekt car" + "<br>");
document.write(car + "<br>");
document.write(person.name + "<br>");
document.write("Ispis objekta u console" + "<br>");
console.log(person);
document.write("<br>Ispis objekta bez navodnika" + "<br>");
var car = {
    modal: "BMW X3",
    color: "white",
    doors: 5
}
console.log(car);
document.write("<h1>Polja i dohvaćanje vrijednosti</h1>" + "<br>");
var colors = ["Red", "Yellow", "Green", "Orange"];
var cities = ["London", "Paris", "New York"];
document.write(colors[0] + "<br>");
document.write(cities[2]);

document.write("<h1>Typeof</h1>" + "<br>");
document.write("typeof 15 = " + typeof 15 + "<br>");
document.write("typeof 42.7 = " + typeof 42.7 + "<br>");
document.write("typeof 2.5e-4 = " + typeof 2.5e-4 + "<br>");
document.write("typeof Infinity = " + typeof Infinity + "<br>");
document.write("typeof NaN (not a number) " + typeof NaN + "<br>");
document.write("typeof '' =" + typeof '' + "<br>");
document.write("typeof 'hello' =" + typeof 'hello' + "<br>");
document.write("typeof '12' =" + typeof '12' + "<br>");
document.write("typeof true = " + typeof true + "<br>");
document.write("typeof false =" + typeof false + "<br>");
document.write("typeof undefined =" + typeof undefined + "<br>");
document.write("typeof undeclaredVariable = " + typeof undeclaredVariable + "<br>");
document.write("typeof Null =" + typeof Null + "<br>");

document.write("typeof { name: \"John\", age: 18 } =" + typeof { name: "John", age: 18 } + "<br>");
document.write("typeof [1, 2, 4] = " + typeof [1, 2, 4] + "<br>");
document.write("typeof function () { }); = " + typeof function () { });


document.write("<h1>Aritmetički operatori</h1>" + "<br>");
var x = 10;
var y = 4;
document.write(x + y);
document.write("<br>");
document.write(x - y);
document.write("<br>");
document.write(x * y);
document.write("<br>");
document.write(x / y);
document.write("<br>");
document.write(x % y);


document.write("<h1>Dodjeljivanje vrijednosti</h1>" + "<br>");
var x;
x = 10;
document.write(x + "<br>");
x = 20;
x += 30;
document.write(x + "<br>");
x = 50;
x -= 20;
document.write(x + "<br>");
x = 5;
x *= 25;
document.write(x + "<br>");
x = 50;
x /= 10;
document.write(x + "<br>");
x = 100;
x %= 15;
document.write(x);


document.write("<h1>Da li je prijestupna godina</h1>" + "<br>");
var year = 2019;
// Djeljivo s 400 ili s 4, ali ne  100
if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0))) {
    document.write(year + " je prijestupna.");
} else {
    document.write(year + " nije prijestupna.");
}

document.write("<h1>Operatori usporedbe</h1>" + "<br>");
var x = 25;
var y = 35;
var z = "25";

document.write(x == z);  
document.write("<br>");
document.write(x === z); 
document.write("<br>");
document.write(x != y);  
document.write("<br>");
document.write(x !== z); 
document.write("<br>");
document.write(x < y);   
document.write("<br>");
document.write(x > y);   
document.write("<br>");
document.write(x <= y);  
document.write("<br>");
document.write(x >= y);  








